﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GuestBook.Data;

namespace GuestBook.Entity.Messages
{
    public class Message : BaseContent
    {
        //[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }

        [MaxLength(100), Required(ErrorMessage = "Введите имя")]
        public string FIO { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(1000), Required(ErrorMessage = "Введите сообщение")]
        public string Text { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
