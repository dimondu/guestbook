using GuestBook.Data;
using GuestBook.Entity.Messages;

namespace GuestBook.Service.Messages
{
    public class CreateMessageResponse : BaseContent, IMappedFrom<Message>
    {
    }
}