using System;
using GuestBook.Data;

namespace GuestBook.Service.Messages
{
    public class CreateMessageRequest : BaseContent
    {
        public string FIO { get; set; }
        public string Text { get; set; }
        public DateTime DateAdded { get; set; }
        public CreateMessageRequest() : base()
        {
        }
    }
}