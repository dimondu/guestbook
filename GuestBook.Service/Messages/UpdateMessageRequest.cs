using System;
using GuestBook.Data;
using GuestBook.Entity.Messages;

namespace GuestBook.Service.Messages
{
    public class UpdateMessageRequest : BaseContent, IMappedFrom<Message>
    {
        public string FIO { get; set; }
        public string Text { get; set; }
        public DateTime DateAdded { get; set; }
    }
}