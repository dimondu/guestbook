using System;

namespace GuestBook.Service.Messages
{
    public interface IMessageService
    {
        System.Collections.Generic.IList<MessageListItem> GetMessages();
        void Delete(Guid id);
        GetMessageResponse Get(Guid id);
        CreateMessageResponse Create(CreateMessageRequest request);
        void Update(UpdateMessageRequest request);
    }
}