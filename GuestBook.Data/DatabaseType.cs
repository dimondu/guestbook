﻿namespace GuestBook.Context
{
    public enum DatabaseType
    {
        MSSQL,
        MongoDB,
        ElasticSearch
    }
}