﻿using System;

namespace GuestBook.Context.MSSQL
{
    [Flags]
    public enum IOMode
    {
        Read,
        Write
    }
}