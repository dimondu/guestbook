﻿using System.Collections.Generic;

namespace GuestBook.Data
{
    public interface IBaseRepository<TEntity>
    {
        TEntity GetById(string id, string includes = "");
        TResult GetById<TResult>(string id) where TResult : IMappedFrom<TEntity>;
        void Add(TEntity item);
        void Delete(string id);
        void Update(TEntity item);
        IList<TResult> GetItems<TResult>(string include = "") where TResult : IMappedFrom<TEntity>;
    }
}
