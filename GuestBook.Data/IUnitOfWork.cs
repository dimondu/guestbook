﻿using System;
using GuestBook.Data;

public interface IUnitOfWork : IDisposable
{
    IDbContext Context { get; }
    void Commit();
}