﻿using GuestBook.Data;

namespace GuestBook.Context.MSSQL
{
    public interface IMSSQLDbContext : IDbContext
    {
        IDbSet<TEntity> GetDbSet<TEntity>() where TEntity : class, IBaseEntity<System.Guid>;
    }
}
