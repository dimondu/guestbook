﻿namespace GuestBook.Data
{
    public static class UtilHelper
    {
        public static string ToKey(string str)
        {
            return string.IsNullOrWhiteSpace(str) 
                ? string.Empty 
                : str.Replace(" ", "_");
        }
    }
}
