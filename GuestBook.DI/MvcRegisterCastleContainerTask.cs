﻿using System.Web;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using GuestBook.Common;
using GuestBook.Data;
using GuestBook.Data.Impl;

namespace GuestBook.DI
{
    public class MvcRegisterCastleContainerTask : BaseTask<TaskArgument<HttpApplication>>, IApplicationStartedTask<TaskArgument<HttpApplication>>
    {
        public MvcRegisterCastleContainerTask() : base(ApplicationType.MVC | ApplicationType.WebApi) { }
        public override void Execute(TaskArgument<System.Web.HttpApplication> taskArg)
        {
            IWindsorContainer windsorContainer = new WindsorContainer();
            RegisterInjector(windsorContainer);
            RegisterControllerFactory(windsorContainer);

            IBaseContainer baseContainer = new CastleContainer(windsorContainer);
            IoC.CreateInstance(baseContainer);

            AssemblyHelper.ExecuteTasks<IBootstrapper, IBaseContainer>(IoC.Container);
        }

        private void RegisterInjector(IWindsorContainer container)
        {
            container.Register(
                Component.For<IWindsorContainer>()
                    .Instance(container));
        }

        private static void RegisterControllerFactory(IWindsorContainer container)
        {
            var controllerFactory = new ControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}

