﻿using GuestBook.Common;
using GuestBook.Data;
using GuestBook.Repository.Impl.Messages;
using GuestBook.Repository.Messages;
using GuestBook.Service.Impl.Message;
using GuestBook.Service.Messages;

namespace GuestBook.DI
{
    public class Bootstrap : BaseTask<IBaseContainer>, IBootstrapper
    {
        public Bootstrap() : base(ApplicationType.All)
        {

        }

        public override void Execute(IBaseContainer context)
        {
            context.RegisterSingleton<IMessageRepository, MessageRepository>();

            context.RegisterSingleton<IMessageService, MessageService>();
        }
    }
}
