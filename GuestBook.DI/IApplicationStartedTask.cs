﻿namespace GuestBook.DI
{
    public interface IApplicationStartedTask<TContext> : IBaseTask<TContext>
    {
    }

    public interface IApplicationReadyTask<TContext> : IBaseTask<TContext>
    {
    }
}
