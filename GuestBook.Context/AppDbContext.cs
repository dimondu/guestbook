﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuestBook.Context.MSSQL;
using GuestBook.Data;
using GuestBook.Entity.Messages;

namespace GuestBook.Context
{
    public class AppDbContext : MSSQLDbContext
    {
        public AppDbContext(IOMode mode = IOMode.Read) : base(new MSSQLConnectionString(), mode)
        {
            Database.SetInitializer<AppDbContext>(new DropCreateDatabaseIfModelChanges<AppDbContext>());
        }

        public DbSet<Message> Messages { get; set; }

        IList<OnContextSaveChange> saveChangeEvents;
    }
}
