﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GuestBook.Context;
using GuestBook.Data;
using GuestBook.Data.Impl;
using GuestBook.Repository.Messages;
using GuestBook.Service.Messages;

namespace GuestBook.Service.Impl.Message
{
    public class MessageService : IMessageService
    {
        public IList<MessageListItem> GetMessages()
        {
            using (IUnitOfWork uow = new UnitOfWork(new AppDbContext()))
            {
                var repository = IoC.Container.Resolve<IMessageRepository>(uow);
                return repository.GetItems<MessageListItem>();
            }
        }

        public void Delete(Guid id)
        {
            ValidateDeleteRequest(id);
            using (IUnitOfWork uow = new UnitOfWork(new AppDbContext()))
            {
                IMessageRepository repository = IoC.Container.Resolve<IMessageRepository>(uow);
                repository.Delete(id.ToString());
                uow.Commit();
            }
        }

        private void ValidateDeleteRequest(Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                throw new ArgumentException("idIsInvalid");
            }
            var repository = IoC.Container.Resolve<MessageListItem>();
            if (repository.GetById(id.ToString()) == null)
            {
                throw new ValidationException("itemNotExist");
            }
        }

        public IList<MessageListItem> GetMessageItems()
        {
            IMessageRepository repository = IoC.Container.Resolve<IMessageRepository>();
            return repository.GetItems<MessageListItem>();
        }

        public GetMessageResponse Get(Guid id)
        {
            var repository = IoC.Container.Resolve<IMessageRepository>();
            var item = repository.GetById(id.ToString());
            var response = ObjectHelper.Convert<GetMessageResponse>(item);
            return response;
        }

        public CreateMessageResponse Create(CreateMessageRequest request)
        {
            ValidateCreateRequest(request);
            using (IUnitOfWork uow = new UnitOfWork(new AppDbContext()))
            {
                var repository = IoC.Container.Resolve<IMessageRepository>(uow);
                var item = GetMessageFromRequest(request);
                repository.Add(item);
                uow.Commit();
                return ObjectHelper.Convert<CreateMessageResponse>(item);
            }

        }

        private Entity.Messages.Message GetMessageFromRequest(CreateMessageRequest request)
        {
            var item = new Entity.Messages.Message
            {
                Id = request.Id,
                Name = request.Name,
                Key = UtilHelper.ToKey(request.Name),
                Text = request.Text,
                DateAdded = request.DateAdded
            };
            return item;
        }

        private void ValidateCreateRequest(CreateMessageRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new ValidationException("productManagement.addOrUpdateProduct.validation.nameIsRequired");
            }
            IMessageRepository repository = IoC.Container.Resolve<IMessageRepository>();
            if (repository.GetById(request.Id.ToString()) != null)
            {
                throw new ValidationException("nameAlreadyExisted");
            }
        }

        public void Update(UpdateMessageRequest request)
        {
            ValidateUpdateRequest(request);
            using (IUnitOfWork uow = new UnitOfWork(new AppDbContext()))
            {
                var repository = IoC.Container.Resolve<IMessageRepository>(uow);
                var item = repository.GetById(request.Id.ToString());
                item.Name = request.Name;
                item.Key = UtilHelper.ToKey(request.Name);
                item.FIO = request.FIO;
                item.Text = request.Text;
                item.DateAdded = request.DateAdded;
                item.Description = request.Description;
                repository.Update(item);
                uow.Commit();
            }
        }
        private void ValidateUpdateRequest(UpdateMessageRequest request)
        {
            if (request.Id == null || request.Id == Guid.Empty)
            {
                throw new ValidationException("idIsInvalid");
            }
            var repository = IoC.Container.Resolve<IMessageRepository>();
            var item = repository.GetById(request.Id.ToString());
            if (item == null)
            {
                throw new ValidationException("itemNotExist");
            }
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new ValidationException("nameIsRequired");
            }
            item = repository.GetById(request.Id.ToString());
            if (item != null && item.Id != request.Id)
            {
                throw new ValidationException("nameAlreadyExisted");
            }
        }
    }
}
