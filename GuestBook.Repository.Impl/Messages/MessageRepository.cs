﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuestBook.Context.MSSQL;
using GuestBook.Data;
using GuestBook.Data.Impl;
using GuestBook.Entity.Messages;
using GuestBook.Repository.Messages;

namespace GuestBook.Repository.Impl.Messages
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        public MessageRepository(IMSSQLDbContext context) : base(context)
        {
        }

        public MessageRepository(IUnitOfWork uow) : base(uow.Context as IMSSQLDbContext)
        {
        }
    }
}
