﻿using System.Configuration;

namespace GuestBook.Common.Configurations
{
    public class AuthenticationElement : ConfigurationElement
    {
        [ConfigurationProperty("tokenExpiredAfterInMinute")]
        public double TokenExpiredAfterInMinute => (double)this["tokenExpiredAfterInMinute"];
    }
}
