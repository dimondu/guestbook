﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuestBook.Common.Http;
using GuestBook.Data.Impl;
using GuestBook.Models;
using GuestBook.Service.Messages;

namespace GuestBook.Controllers
{
    public class HomeController : Controller
    {
        GuestBookModel db = new GuestBookModel();

        [HttpGet]
        [Route("")]
        public IResponseData<IList<MessageListItem>> Index()
        {
            IResponseData<IList<MessageListItem>> response = new ResponseData<IList<MessageListItem>>();
            try
            {
                var service = IoC.Container.Resolve<IMessageService>();
                var items = service.GetMessages();
                response.SetData(items);
            }
            catch (ValidationException ex)
            {
                response.SetErrors(ex.Errors);
                response.SetStatus(System.Net.HttpStatusCode.PreconditionFailed);
            }
            return response;

            //var entries = db.GuestbookEntries;
            //return View(entries);
        }

        public ActionResult CreateMessagePartial()
        {
            return PartialView();
        }


        //[HttpPost]
        //public ActionResult AddComment(string comment)
        //{
        //    _comments.Add(comment);
        //    if (Request.IsAjaxRequest())
        //    {
        //        ViewBag.Comment = comment;
        //        return PartialView();
        //    }
        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        public ActionResult CreateMessagePartial(GuestbookEntry entry)
        {
            if (ModelState.IsValid)
            {
                entry.DateAdded = DateTime.Now;
                db.GuestbookEntries.Add(entry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                // validation error
                return View();
            }
        }


    }
}