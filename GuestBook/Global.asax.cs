﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using GuestBook.Common;
using GuestBook.DI;

namespace GuestBook
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            OnApplicationStarted();
        }


        private void OnApplicationStarted()
        {
            var taskArg = new TaskArgument<HttpApplication>(this, ApplicationType.MVC);
            AssemblyHelper.ExecuteTasks<IApplicationStartedTask<TaskArgument<HttpApplication>>, TaskArgument<HttpApplication>>(taskArg);
            AssemblyHelper.ExecuteTasks<IApplicationReadyTask<TaskArgument<HttpApplication>>, TaskArgument<HttpApplication>>(taskArg, true);
        }
    }
}
