using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GuestBook.Models
{
    public class GuestbookEntry
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(30), Required(ErrorMessage = "������� ���")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(1000), Required(ErrorMessage = "������� ���������")]
        public string Message { get; set; }
        public DateTime DateAdded { get; set; }
    }
}