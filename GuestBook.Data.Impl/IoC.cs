﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuestBook.Data.Impl
{
    public static class IoC
    {
        public static IBaseContainer Container { get; set; }
        public static void CreateInstance(IBaseContainer baseContainer)
        {
            IoC.Container = baseContainer;
        }
    }
}
