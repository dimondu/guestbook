﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using GuestBook.Context.MSSQL;

namespace GuestBook.Data.Impl
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IBaseEntity<System.Guid>
    {
        public IDbSet<TEntity> DbSet { get; private set; }
        public BaseRepository(IMSSQLDbContext context)
        {
            //throw new Exception("Function was not implemented");
            this.DbSet = context.GetDbSet<TEntity>();
        }

        public virtual TEntity GetById(string id, string includes = "")
        {
            return this.DbSet.Get(id, includes);
        }

        public virtual TEntity GetById(string id)
        {
            return this.GetById(id, "");
        }

        public virtual TResult GetById<TResult>(string id) where TResult : IMappedFrom<TEntity>
        {
            TEntity entity = this.GetById(id);
            return AutoMapper.Mapper.Map<TResult>(entity);
        }

        public virtual IList<TResult> GetItems<TResult>(string include = "") where TResult : IMappedFrom<TEntity>
        {
            return this.DbSet.AsQueryable(include).ProjectTo<TResult>().ToList();
        }

        public virtual void Add(TEntity item)
        {
            this.DbSet.Add(item);
        }

        public virtual void Delete(string id)
        {
            this.DbSet.Delete(id);
        }

        public virtual void Update(TEntity item)
        {
            this.DbSet.Update(item);
        }

    }
}
