﻿namespace GuestBook.Data.Impl
{
    public static class ObjectHelper
    {
        public static TEntity Convert<TEntity>(object obj)
        {
            return AutoMapper.Mapper.Map<TEntity>(obj);
        }
    }
}
