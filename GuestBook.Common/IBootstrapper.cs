﻿using GuestBook.Data;
using GuestBook.DI;

namespace GuestBook.Common
{
    public interface IBootstrapper : IBaseTask<IBaseContainer>
    {
    }

}
