﻿using System;

namespace GuestBook.Common
{
    [Flags]
    public enum ApplicationType
    {
        Console = 1,
        MVC = 2,
        WebApi = 4,
        All = 7
    }
}