﻿using System.Configuration;
using GuestBook.Context;

namespace GuestBook.Common.Configurations
{
    public class ConnectionStringElement : ConfigurationElement
    {

        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name => (string)this["name"];

        [ConfigurationProperty("database")]
        public string Database => (string)this["database"];

        [ConfigurationProperty("server")]
        public string Server => (string)this["server"];

        [ConfigurationProperty("port")]
        public int Port => (int)this["port"];

        [ConfigurationProperty("userName")]
        public string UserName => (string)this["userName"];

        [ConfigurationProperty("password")]
        public string Password => (string)this["password"];

        [ConfigurationProperty("dbType")]
        public DatabaseType DbType => (DatabaseType)this["dbType"];

        [ConfigurationProperty("default")]
        public bool IsDefault => (bool)this["default"];
    }
}