﻿using System.Configuration;

namespace GuestBook.Common.Configurations
{
    public class MailElement : ConfigurationElement
    {
        [ConfigurationProperty("server")]
        public string Server => (string)this["server"];

        [ConfigurationProperty("port")]
        public int Port => (int)this["port"];

        [ConfigurationProperty("ssl")]
        public bool Ssl => (bool)this["ssl"];

        [ConfigurationProperty("username")]
        public string Username => (string)this["username"];

        [ConfigurationProperty("password")]
        public string Password => (string)this["password"];

        [ConfigurationProperty("displayName")]
        public string DisplayName => (string)this["displayName"];

        [ConfigurationProperty("defaultSender")]
        public string DefaultSender => (string)this["defaultSender"];
    }
}
