﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using GuestBook.Context;
using GuestBook.Context.MSSQL;

namespace GuestBook.Common.Configurations
{
    public class ConnectionString : IConnectionString
    {
        public ConnectionString() { }
        public ConnectionString(DatabaseType dbType, string connectionName = "")
        {
            ConnectionStringElement connectionString = Configuration.Current
                .Databases.ToList().FirstOrDefault(
                    item => item.DbType == dbType 
                    && ((string.IsNullOrWhiteSpace(connectionName) 
                    && item.IsDefault)
                    || item.Name == connectionName));

            if (string.IsNullOrWhiteSpace(connectionName) && connectionString == null)
            {
                throw new ValidationException("ConnectionStringNoDefaultItem");
            }
            if (!string.IsNullOrWhiteSpace(connectionName) && connectionString == null)
            {
                throw new ValidationException("ConnectionStringInvalidName");
            }
            Apply(connectionString);
        }

        private void Apply(ConnectionStringElement connectionString)
        {
            this.Name = connectionString.Name;
            this.Database = connectionString.Database;
            this.Server = connectionString.Server;
            this.Port = connectionString.Port;
            this.UserName = connectionString.UserName;
            this.Password = connectionString.Password;
            this.DbType = connectionString.DbType;
        }
        public string Name { get; set; }
        public string Database { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DatabaseType DbType { get; set; }

    }
}
