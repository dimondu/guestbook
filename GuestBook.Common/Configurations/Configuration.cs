﻿using System.Configuration;

namespace GuestBook.Common.Configurations
{
    public class Configuration : System.Configuration.ConfigurationSection
    {
        private static Configuration _current;
        public static Configuration Current => _current ?? (_current =
                                                   (Configuration) ConfigurationManager.GetSection("appconfiguration"));

        [ConfigurationProperty("databases", IsDefaultCollection=false)]
        [ConfigurationCollection(typeof(DatabasesElement),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public DatabasesElement Databases => (DatabasesElement)this["databases"];

        [ConfigurationProperty("mail")]
        public MailElement Mail => (MailElement)this["mail"];
    }
}
