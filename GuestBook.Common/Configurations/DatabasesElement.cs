﻿using System.Configuration;

namespace GuestBook.Common.Configurations
{
    public class DatabasesElement : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ConnectionStringElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConnectionStringElement)element).Name;
        }

        public ConnectionStringElement this[int index]
        {
            get => (ConnectionStringElement)BaseGet(index);
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
        public new ConnectionStringElement this[string Name] => (ConnectionStringElement)BaseGet(Name);

        public int IndexOf(ConnectionStringElement url)
        {
            return BaseIndexOf(url);
        }


        public void Add(ConnectionStringElement url)
        {
            BaseAdd(url);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(ConnectionStringElement url)
        {
            if (BaseIndexOf(url) >= 0)
            {
                BaseRemove(url.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }

        public System.Collections.Generic.IList<ConnectionStringElement> ToList()
        {
            System.Collections.Generic.IList<ConnectionStringElement> items = new System.Collections.Generic.List<ConnectionStringElement>();
            object[] keys = BaseGetAllKeys();
            foreach (object key in keys)
            {
                var item = (ConnectionStringElement)BaseGet(key);
                items.Add(item);
            }
            return items;
        }
    }
}
