﻿using System.Collections.Generic;

namespace GuestBook.Common.Http
{
    public interface IValidationException
    {
        IList<ValidationError> Errors { get; set; }
        void Add(ValidationError error);
    }
}