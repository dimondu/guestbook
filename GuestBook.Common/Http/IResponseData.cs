﻿using System.Collections.Generic;

namespace GuestBook.Common.Http
{
    public interface IResponseData<DataType>
    {
        void SetStatus(System.Net.HttpStatusCode httpStatusCode);
        void SetErrors(IList<ValidationError> errors);
        void SetData(DataType data);
    }
}
