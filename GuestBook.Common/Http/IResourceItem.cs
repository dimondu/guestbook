﻿using System.Collections.Generic;

namespace GuestBook.Common.Http
{
    public interface IResourceItem
    {
        string Key { get; set; }
        string Message { get; set; }
        IList<string> Params { get; set; }
    }
}