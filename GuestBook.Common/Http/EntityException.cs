﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GuestBook.Common.Http
{
    public class EntityException : Exception
    {
        public IEnumerable<ValidationResult> Errors { get; set; }

        public EntityException(IEnumerable<ValidationResult> errors)
        {
            this.Errors = errors;
        }

        public EntityException(ValidationResult error)
        {
            this.Errors = new List<ValidationResult>() { error };
        }
    }
}