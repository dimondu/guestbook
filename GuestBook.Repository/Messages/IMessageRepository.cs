﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuestBook.Data;
using GuestBook.Entity.Messages;

namespace GuestBook.Repository.Messages
{
    public interface IMessageRepository : IBaseRepository<Message>
    {
    }
}
